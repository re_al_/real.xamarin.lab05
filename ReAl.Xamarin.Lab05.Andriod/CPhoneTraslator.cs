﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace ReAl.Xamarin.Lab05.Andriod
{
    public class CPhoneTraslator
    {
        private string strLetras = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        private string strNumero = "22233344455566677778889999";

        public string strToNumber(string strTexto)
        {
            var strNumeroFinal = new StringBuilder();

            if (!string.IsNullOrWhiteSpace(strTexto))
            {
                strTexto = strTexto.ToUpper();
                foreach (var c in strTexto)
                {
                    if (strNumero.Contains(c))
                        strNumeroFinal.Append(c);
                    else
                    {
                        var index = strLetras.IndexOf(c);
                        if (index >= 0)
                            strNumeroFinal.Append(strNumero[index]);
                    }
                }
            }

            return strNumeroFinal.ToString();
        }
    }
}