﻿using Android.App;
using Android.Content;
using Android.Net;
using Android.Widget;
using Android.OS;
using Android.Provider;

namespace ReAl.Xamarin.Lab05.Andriod
{
    [Activity(Label = "Phone App", MainLauncher = true, Icon = "@drawable/icon")]
    public class MainActivity : Activity
    {
        private EditText txtNumero;
        private Button btnConvertir;
        private Button btnLlamar;
        private TextView txtMensaje;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            // Set our view from the "main" layout resource
            SetContentView (Resource.Layout.Main);

            txtNumero = FindViewById<EditText>(Resource.Id.txtPhoneNumber);
            txtMensaje = FindViewById<TextView>(Resource.Id.txtMensaje);
            btnConvertir = FindViewById<Button>(Resource.Id.btnConvertir);
            btnLlamar = FindViewById<Button>(Resource.Id.btnLlamar);

            btnLlamar.Enabled = false;
            var strNumeroIntroducido = string.Empty;

            btnConvertir.Click += (sender, args) =>
            {
                var miTraductor = new CPhoneTraslator();
                strNumeroIntroducido = miTraductor.strToNumber(txtNumero.Text);

                if (string.IsNullOrWhiteSpace(strNumeroIntroducido))
                {
                    btnLlamar.Text = "Llamar";
                    btnLlamar.Enabled = false;
                }
                else
                {
                    btnLlamar.Text = "Llamar al " + strNumeroIntroducido;
                    btnLlamar.Enabled = true;
                }
            };

            btnLlamar.Click += (sender, args) =>
            {
                //Llamamos
                var callDialog = new AlertDialog.Builder(this);
                callDialog.SetMessage($"Llamar al numero {strNumeroIntroducido}?");
                callDialog.SetNeutralButton("Llamar", delegate(object o, DialogClickEventArgs eventArgs)
                {
                    //Crear un Intent
                    var callIntent = new Android.Content.Intent(Intent.ActionCall);
                    callIntent.SetData(Uri.Parse($"tel:{strNumeroIntroducido}"));
                    StartActivity(callIntent);
                });
                callDialog.SetNegativeButton("Cancelar", delegate(object o, DialogClickEventArgs eventArgs) { });
                callDialog.Show();
            };

            Validate();
        }

        private async void Validate()
        {
            var client = new SALLab05.ServiceClient();
            var miCorreo = "xxxxxxx@gmail.com";
            var miPass = "PASSWORD";
            var miDeviceId = Settings.Secure.GetString(ContentResolver, Settings.Secure.AndroidId);

            SALLab05.ResultInfo resultado = await client.ValidateAsync(miCorreo, miPass, miDeviceId);

            txtMensaje.Text = resultado.Status + "\r\n\r\n" + resultado.Fullname + "\r\n" + resultado.Token;
        }
    }
}

